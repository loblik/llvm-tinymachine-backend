//===-- TinyTargetMachine.cpp - Define TargetMachine for Tiny ---------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Top-level implementation for the Tiny target.
//
//===----------------------------------------------------------------------===//

#include "TinyTargetMachine.h"
#include "Tiny.h"
#include "llvm/CodeGen/Passes.h"
#include "llvm/CodeGen/TargetLoweringObjectFileImpl.h"
#include "llvm/CodeGen/TargetPassConfig.h"
#include "llvm/MC/MCAsmInfo.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Support/TargetRegistry.h"
using namespace llvm;

extern "C" void LLVMInitializeTinyTarget() {
  // Register the target.
  RegisterTargetMachine<TinyTargetMachine> X(TheTinyTarget);
}

static Reloc::Model getEffectiveRelocModel(Optional<Reloc::Model> RM) {
  if (!RM.hasValue())
    return Reloc::Static;
  return *RM;
}

TinyTargetMachine::TinyTargetMachine(const Target &T, const Triple &TT,
                                         StringRef CPU, StringRef FS,
                                         const TargetOptions &Options,
                                         Optional<Reloc::Model> RM,
                                         CodeModel::Model CM,
                                         CodeGenOpt::Level OL)
    : LLVMTargetMachine(T, "e-m:e-p:32:32-i32:32:32-n32", TT, CPU, FS,
                        Options, getEffectiveRelocModel(RM), CM, OL),
                        TLOF(make_unique<TargetLoweringObjectFileELF>()),
      Subtarget(TT, CPU, FS, *this) {
  initAsmInfo();
}

namespace {
/// Tiny Code Generator Pass Configuration Options.
class TinyPassConfig : public TargetPassConfig {
public:
  TinyPassConfig(TinyTargetMachine *TM, PassManagerBase &PM)
    : TargetPassConfig(TM, PM) {}

  TinyTargetMachine &getTinyTargetMachine() const {
    return getTM<TinyTargetMachine>();
  }

  bool addInstSelector() override;
  void addPreEmitPass() override;
};
} // namespace

TargetPassConfig *TinyTargetMachine::createPassConfig(PassManagerBase &PM) {
  return new TinyPassConfig(this, PM);
}

bool TinyPassConfig::addInstSelector() {
  // Install an instruction selector.
  addPass(createTinyISelDag(getTinyTargetMachine(), getOptLevel()));
  return false;
}

void TinyPassConfig::addPreEmitPass() {
  // Must run branch selection immediately preceding the asm printer.
  addPass(createTinyBranchSelectionPass());
}


