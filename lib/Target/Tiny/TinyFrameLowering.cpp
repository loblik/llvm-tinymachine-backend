//===-- TinyFrameLowering.cpp - Tiny Frame Information ----------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the Tiny implementation of TargetFrameLowering class.
//
//===----------------------------------------------------------------------===//

#include "TinyFrameLowering.h"
#include "TinyInstrInfo.h"
#include "TinyMachineFunctionInfo.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineModuleInfo.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Target/TargetOptions.h"

using namespace llvm;

bool TinyFrameLowering::hasFP(const MachineFunction &MF) const {
  const MachineFrameInfo *MFI = MF.getFrameInfo();
  const TargetRegisterInfo *TRI = MF.getSubtarget().getRegisterInfo();

  //errs() << "STACK: disableFPElim: " << MF.getTarget().Options.DisableFramePointerElim(MF) << "\n";
  return (MF.getTarget().Options.DisableFramePointerElim(MF) ||
          MF.getFrameInfo()->hasVarSizedObjects() ||
          MFI->isFrameAddressTaken() ||
          TRI->needsStackRealignment(MF));
}

bool TinyFrameLowering::hasReservedCallFrame(const MachineFunction &MF) const {
  return !MF.getFrameInfo()->hasVarSizedObjects();
}

void TinyFrameLowering::emitPrologue(MachineFunction &MF, MachineBasicBlock &MBB) const {
  //MachineBasicBlock &MBB = MF.front();   // Prolog goes in entry BB
  MachineFrameInfo *MFI = MF.getFrameInfo();
  TinyMachineFunctionInfo *TinyFI = MF.getInfo<TinyMachineFunctionInfo>();
  const TinyInstrInfo &TII =
    *static_cast<const TinyInstrInfo*>(MF.getSubtarget().getInstrInfo());
  const TargetRegisterInfo *TRI = MF.getSubtarget().getRegisterInfo();

  MachineBasicBlock::iterator MBBI = MBB.begin();
  DebugLoc DL = MBBI != MBB.end() ? MBBI->getDebugLoc() : DebugLoc();

  // Get the number of bytes to allocate from the FrameInfo.
  uint64_t StackSize = MFI->getStackSize();

  errs() << "\nSTACK: emitPrologue\n";
  errs() << "STACK: getStackSize: " << StackSize << "\n";
  errs() << "STACK: getCalleSavedFrameSize: " << TinyFI->getCalleeSavedFrameSize() << "\n";
  errs() << "STACK: getMaxCallFrameSize: " << MFI->getMaxCallFrameSize() << "\n";
  errs() << "STACK: getLocalFrameSize: " << MFI->getLocalFrameSize() << "\n";
  errs() << "STACK: needsStackRealignment: " << TRI->needsStackRealignment(MF) << "\n";

  if (MFI->hasVarSizedObjects())
    assert(0 && "var sized objects on stack, not supported yet");

  uint64_t MaxAlign = MFI->getMaxAlignment();
  uint64_t NumBytes = StackSize;

  // exit early if there is no epilog needed
  if (!NumBytes || (hasFP(MF) && NumBytes == 4))
    return;

  if (hasFP(MF)) {
    errs() << "STACK: hasFP\n";
    // Calculate required stack adjustment
    //uint64_t FrameSize = StackSize - 2;
    //NumBytes = FrameSize - TinyFI->getCalleeSavedFrameSize();

    // Get the offset of the stack slot for the EBP register... which is
    // guaranteed to be the last slot by processFunctionBeforeFrameFinalized.
    // Update the frame offset adjustment.

    /* FIXME: should be set to difference between FP and SP */
    //MFI->setOffsetAdjustment(-NumBytes);

    // Save FPW into the appropriate stack slot...
    //BuildMI(MBB, MBBI, DL, TII.get(Tiny::PUSH16r))
    //  .addReg(Tiny::FPW, RegState::Kill);
    BuildMI(MBB, MBBI, DL, TII.get(Tiny::ST_ri), Tiny::FP).addReg(Tiny::SP).addImm(-4);

    // Update FPW with the new base value...
    //BuildMI(MBB, MBBI, DL, TII.get(Tiny::MOV16rr), Tiny::FPW)
    //  .addReg(Tiny::SPW);
    BuildMI(MBB, MBBI, DL, TII.get(Tiny::MOV), Tiny::FP).addReg(Tiny::SP);

    // Mark the FramePtr as live-in in every block except the entry.
    for (MachineFunction::iterator I = std::next(MF.begin()), E = MF.end();
            I != E; ++I)
        I->addLiveIn(Tiny::FP);

  } else {
    //NumBytes = StackSize - TinyFI->getCalleeSavedFrameSize();
    NumBytes = StackSize;
    errs() << "STACK: doesNotHaveFP, NumBytes: " << NumBytes << "\n";
  }

  // Skip the callee-saved push instructions.
  /*
  while (MBBI != MBB.end() && (MBBI->getOpcode() == Tiny::PUSH16r))
    ++MBBI;
  */
/*
  if (NumBytes) { // adjust stack pointer: SPW -= numbytes
    // If there is an SUB16ri of SPW immediately before this instruction, merge
    // the two.
    //NumBytes -= mergeSPUpdates(MBB, MBBI, true);
    // If there is an ADD16ri or SUB16ri of SPW immediately after this
    // instruction, merge the two instructions.
    // mergeSPUpdatesDown(MBB, MBBI, &NumBytes);

    if (NumBytes) {
      MachineInstr *MI =
        BuildMI(MBB, MBBI, DL, TII.get(Tiny::SUB16ri), Tiny::SPW)
        .addReg(Tiny::SPW).addImm(NumBytes);
      // The SRW implicit def is dead.
      MI->getOperand(3).setIsDead();
    }
  }
*/


  if (MBBI != MBB.end())
    DL = MBBI->getDebugLoc();

  BuildMI(MBB, MBBI, DL, TII.get(Tiny::LDC), Tiny::R1).addImm(NumBytes);
  BuildMI(MBB, MBBI, DL, TII.get(Tiny::SUB), Tiny::SP).addReg(Tiny::SP).addReg(Tiny::R1);

  if (TRI->needsStackRealignment(MF)) {
      BuildMI(MBB, MBBI, DL, TII.get(Tiny::LDC), Tiny::R1)
          .addImm(-MaxAlign);
      BuildMI(MBB, MBBI, DL, TII.get(Tiny::AND), Tiny::SP)
          .addReg(Tiny::SP).addReg(Tiny::R1);
  }
}

void TinyFrameLowering::emitEpilogue(MachineFunction &MF,
                                       MachineBasicBlock &MBB) const {
  const MachineFrameInfo *MFI = MF.getFrameInfo();
  //TinyMachineFunctionInfo *TinyFI = MF.getInfo<TinyMachineFunctionInfo>();
  const TinyInstrInfo &TII =
    *static_cast<const TinyInstrInfo*>(MF.getSubtarget().getInstrInfo());

  MachineBasicBlock::iterator MBBI = MBB.getLastNonDebugInstr();
  unsigned RetOpcode = MBBI->getOpcode();
  DebugLoc DL = MBBI->getDebugLoc();

  switch (RetOpcode) {
  case Tiny::RET: break;  // These are ok
  default:
    llvm_unreachable("Can only insert epilog into returning blocks");
  }

  // Get the number of bytes to allocate from the FrameInfo
  uint64_t StackSize = MFI->getStackSize();
  //unsigned CSSize = TinyFI->getCalleeSavedFrameSize();
  uint64_t NumBytes = 0;

  errs() << "\nSTACK: EPILOGUE\n";

  if (hasFP(MF)) {
    // Calculate required stack adjustment
    //uint64_t FrameSize = StackSize - 2;
    //NumBytes = FrameSize - CSSize;
    NumBytes = StackSize;

    errs() << "Epil: " << NumBytes << "\n";

    /* load saved FP from stack (use FP as base since SP can be anywhere if we have
     * dynamic allocas on stack */
    //BuildMI(MBB, MBBI, DL, TII.get(Tiny::POP16r), Tiny::FPW);
  } else
    NumBytes = StackSize;
    //NumBytes = StackSize - CSSize;

  /*
  // Skip the callee-saved pop instructions.
  while (MBBI != MBB.begin()) {
    MachineBasicBlock::iterator PI = std::prev(MBBI);
    unsigned Opc = PI->getOpcode();
    // FIXME: commented out
    if (Opc != Tiny::POP16r && !PI->isTerminator())
      break;
    --MBBI;
  }
  */

  if (MFI->hasVarSizedObjects()) {
    assert(0 && "var sized objects on stack, not supported yet");
  }

  if (!NumBytes || (hasFP(MF) && NumBytes == 4))
    return;

  DL = MBBI->getDebugLoc();

  if (hasFP(MF)) {
      // if FP is used in function we don't know how far SP is since there could
      // be dynamic realignment. so we free the stack by moving FP to SP.
      BuildMI(MBB, MBBI, DL, TII.get(Tiny::MOV),
        Tiny::SP).addReg(Tiny::FP);
      BuildMI(MBB, MBBI, DL, TII.get(Tiny::LD_ri),
        Tiny::FP).addReg(Tiny::FP).addImm(-4);
    } else {
      BuildMI(MBB, MBBI, DL, TII.get(Tiny::LDC),
        Tiny::R1).addImm(NumBytes);
      BuildMI(MBB, MBBI, DL, TII.get(Tiny::ADD),
        Tiny::SP).addReg(Tiny::SP).addReg(Tiny::R1);
  }

  // FIXME: can we also merge some SP updates?
  // If there is an ADD16ri or SUB16ri of SPW immediately before this
  // instruction, merge the two instructLDns.
  //if (NumBytes || MFI->hasVarSizedObjects())
  //  mergeSPUpdatesUp(MBB, MBBI, StackPtr, &NumBytes);
  /*
  if (MFI->hasVarSizedObjects()) {
    BuildMI(MBB, MBBI, DL,
            TII.get(Tiny::MOV16rr), Tiny::SPW).addReg(Tiny::FPW);
    if (CSSize) {
      MachineInstr *MI =
        BuildMI(MBB, MBBI, DL,
                TII.get(Tiny::SUB16ri), Tiny::SPW)
        .addReg(Tiny::SPW).addImm(CSSize);
      // The SRW implicit def is dead.
      MI->getOperand(3).setIsDead();
    }
  } else {
    // adjust stack pointer back: SPW += numbytes
    if (NumBytes) {
      MachineInstr *MI =
        BuildMI(MBB, MBBI, DL, TII.get(Tiny::ADD16ri), Tiny::SPW)
        .addReg(Tiny::SPW).addImm(NumBytes);
      // The SRW implicit def is dead.
      MI->getOperand(3).setIsDead();
    }
  }
  */
}

// FIXME: Can we eleminate these in favour of generic code?
bool
TinyFrameLowering::spillCalleeSavedRegisters(MachineBasicBlock &MBB,
                                           MachineBasicBlock::iterator MI,
                                        const std::vector<CalleeSavedInfo> &CSI,
                                        const TargetRegisterInfo *TRI) const {
  if (CSI.empty())
    return false;

  DebugLoc DL;
  if (MI != MBB.end()) DL = MI->getDebugLoc();

  MachineFunction &MF = *MBB.getParent();
  const TargetInstrInfo &TII = *MF.getSubtarget().getInstrInfo();
  //unsigned basePtr = hasFP(MF) ? Tiny::FP : Tiny::SP;
  TinyMachineFunctionInfo *MFI = MF.getInfo<TinyMachineFunctionInfo>();
  MachineBasicBlock &EntryBlock = *MF.begin();
  /* we have 4byte length registers */
  MFI->setCalleeSavedFrameSize(CSI.size() * 4);

  errs() << "STACK: SPILLING CALLEE SAVED: count: " << CSI.size() << "\n";

  for (int i = CSI.size() - 1; i >= 0; --i) {
    unsigned Reg = CSI[i].getReg();
    // Add the callee-saved register as live-in. It's killed at the spill.
    errs() << "STACK: SPILL FRAMEIDX: " <<
        CSI[i].getFrameIdx() << " REG: " << Reg << "\n";

    //MBB.addLiveIn(Reg);
    //BuildMI(MBB, MI, DL, TII.get(Tiny::ST_ri))
    //    .addReg(Reg, RegState::Kill).addReg(basePtr).addImm(4*i);
    MBB.addLiveIn(Reg);
    const TargetRegisterClass *RC = TRI->getMinimalPhysRegClass(Reg);
    TII.storeRegToStackSlot(EntryBlock, MI, Reg, 1, CSI[i].getFrameIdx(), RC, TRI);
  }
  return true;
}

bool
TinyFrameLowering::restoreCalleeSavedRegisters(MachineBasicBlock &MBB,
                                                 MachineBasicBlock::iterator MI,
                                        const std::vector<CalleeSavedInfo> &CSI,
                                        const TargetRegisterInfo *TRI) const {
  if (CSI.empty())
    return false;

  DebugLoc DL;
  if (MI != MBB.end()) DL = MI->getDebugLoc();

  MachineFunction &MF = *MBB.getParent();
  //MachineBasicBlock *EntryBlock = MF.begin();
  const TargetInstrInfo &TII = *MF.getSubtarget().getInstrInfo();
  //unsigned basePtr = hasFP(MF) ? Tiny::FP : Tiny::SP;
  //assert(0 && "restore callee saved regs");
  for (unsigned i = 0, e = CSI.size(); i != e; ++i) {
    unsigned Reg = CSI[i].getReg();
    const TargetRegisterClass *RC = TRI->getMinimalPhysRegClass(Reg);
    TII.loadRegFromStackSlot(MBB, MI, Reg, CSI[i].getFrameIdx(), RC, TRI);
  }
  //  BuildMI(MBB, MI, DL, TII.get(Tiny::LD_ri), CSI[i]
  //      .getReg()).addReg(basePtr).addImm(4*i);
  return true;
}

bool TinyFrameLowering::assignCalleeSavedSpillSlots(
    MachineFunction &MF, const TargetRegisterInfo *TRI,
    std::vector<CalleeSavedInfo> &CSI) const {
  const TinyRegisterInfo *RegInfo =
      static_cast<const TinyRegisterInfo *>(MF.getSubtarget().getRegisterInfo());

  TinyMachineFunctionInfo *TinyFI = MF.getInfo<TinyMachineFunctionInfo>();

  unsigned SlotSize = 4;
  unsigned CalleeSavedFrameSize = 0;
  MachineFrameInfo *MFI = MF.getFrameInfo();
  int SpillSlotOffset = getOffsetOfLocalArea();

  // if emitPrologue() has to backup FP, we leave some room for it
  if (hasFP(MF))
    SpillSlotOffset += -4;

  for (unsigned i = CSI.size(); i != 0; --i) {
    unsigned Reg = CSI[i - 1].getReg();

    SpillSlotOffset -= SlotSize;
    CalleeSavedFrameSize += SlotSize;
    int SlotIndex = MFI->CreateFixedSpillStackObject(SlotSize, SpillSlotOffset);
    CSI[i - 1].setFrameIdx(SlotIndex);
  }

  TinyFI->setCalleeSavedFrameSize(CalleeSavedFrameSize);

  return true;
}

MachineBasicBlock::iterator
TinyFrameLowering::eliminateCallFramePseudoInstr(MachineFunction &MF, MachineBasicBlock &MBB,
                              MachineBasicBlock::iterator I) const {
    /* R1 is reserved (between callseq_start/end) to be used to help
     * setup call frame, extra free register is needed since we don't
     * have add/sub with immediate to alter the stack pointer */
    MCPhysReg TmpFreeReg = Tiny::R1;

    MachineInstr *Old = I;
    MachineInstr *New1, *New2;
    uint64_t Amount = Old->getOperand(0).getImm();
    const TinyInstrInfo &TII = *static_cast<const TinyInstrInfo*>(MF.getSubtarget().getInstrInfo());

    errs() << "\nSTACK: eliminateCallFramePseudoInstr\n";
    errs() << "STACK: amount :" << Amount << "\n";

    if (!hasReservedCallFrame(MF)) {
        if (I->getOpcode() == TII.getCallFrameSetupOpcode()) {
            printf("STACK: frame setup\n");

            New1 = BuildMI(MF, Old->getDebugLoc(), TII.get(Tiny::LDC), TmpFreeReg)
                .addImm(Amount);
            MBB.insert(I, New1);
            New2 = BuildMI(MF, Old->getDebugLoc(), TII.get(Tiny::SUB), Tiny::SP)
                .addReg(Tiny::SP).addReg(TmpFreeReg);
            MBB.insert(I, New2);
        }
        if (I->getOpcode() == TII.getCallFrameDestroyOpcode()) {
            printf("STACK: frame destroy\n");
            New1 = BuildMI(MF, Old->getDebugLoc(), TII.get(Tiny::LDC), TmpFreeReg)
                .addImm(Amount);
            MBB.insert(I, New1);
            New2 = BuildMI(MF, Old->getDebugLoc(), TII.get(Tiny::SUB), Tiny::SP)
                .addReg(Tiny::SP).addReg(TmpFreeReg);
            MBB.insert(I, New2);
        }
    } else {
        errs() << "STACK: hasReservedCallFrame\n";
        //assert(0 && "has reserved call frame\n");
    }

    MBB.erase(I);

  /*
  const TinyInstrInfo &TII =
    *static_cast<const TinyInstrInfo*>(MF.getTarget().getInstrInfo());
  unsigned StackAlign = getStackAlignment();
  if (!hasReservedCallFrame(MF)) {
    // If the stack pointer can be changed after prologue, turn the
    // adjcallstackup instruction into a 'sub SPW, <amt>' and the
    // adjcallstackdown instruction into 'add SPW, <amt>'
    // TODO: consider using push / pop instead of sub + store / add
    MachineInstr *Old = I;
    uint64_t Amount = Old->getOperand(0).getImm();
    if (Amount != 0) {
      // We need to keep the stack aligned properly.  To do this, we round the
      // amount of space needed for the outgoing arguments up to the next
      // alignment boundary.
      Amount = (Amount+StackAlign-1)/StackAlign*StackAlign;

      MachineInstr *New = nullptr;
      if (Old->getOpcode() == TII.getCallFrameSetupOpcode()) {
        New = BuildMI(MF, Old->getDebugLoc(),
                      TII.get(Tiny::SUB16ri), Tiny::SPW)
          .addReg(Tiny::SPW).addImm(Amount);
      } else {
        assert(Old->getOpcode() == TII.getCallFrameDestroyOpcode());
        // factor out the amount the callee already popped.
        uint64_t CalleeAmt = Old->getOperand(1).getImm();
        Amount -= CalleeAmt;
        if (Amount)
          New = BuildMI(MF, Old->getDebugLoc(),
                        TII.get(Tiny::ADD16ri), Tiny::SPW)
            .addReg(Tiny::SPW).addImm(Amount);
      }

      if (New) {
        // The SRW implicit def is dead.
        New->getOperand(3).setIsDead();

        // Replace the pseudo instruction with a new instruction...
        MBB.insert(I, New);
      }
    }
  } else if (I->getOpcode() == TII.getCallFrameDestroyOpcode()) {
    // If we are performing frame pointer elimination and if the callee pops
    // something off the stack pointer, add it back.
    if (uint64_t CalleeAmt = I->getOperand(1).getImm()) {
      MachineInstr *Old = I;
      MachineInstr *New =
        BuildMI(MF, Old->getDebugLoc(), TII.get(Tiny::SUB16ri),
                Tiny::SPW).addReg(Tiny::SPW).addImm(CalleeAmt);
      // The SRW implicit def is dead.
      New->getOperand(3).setIsDead();

      MBB.insert(I, New);
    }
  }
    */
}

void
TinyFrameLowering::processFunctionBeforeFrameFinalized(MachineFunction &MF,
                                                         RegScavenger *) const {
  // Create a frame entry for the FPW register that must be saved.
  if (hasFP(MF)) {
    int FrameIdx = MF.getFrameInfo()->CreateFixedObject(4, -4, true);
    (void)FrameIdx;
    assert(FrameIdx == MF.getFrameInfo()->getObjectIndexBegin() &&
           "Slot for FPW register must be last in order to be found!");
  }
}
