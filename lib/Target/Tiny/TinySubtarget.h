//===-- TinySubtarget.h - Define Subtarget for the Tiny ----*- C++ -*--===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file declares the Tiny specific subclass of TargetSubtargetInfo.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_TARGET_Tiny_SUBTARGET_H
#define LLVM_TARGET_Tiny_SUBTARGET_H

#include "TinyFrameLowering.h"
#include "TinyInstrInfo.h"
#include "TinyISelLowering.h"
#include "TinyRegisterInfo.h"
#include "llvm/CodeGen/SelectionDAGTargetInfo.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Target/TargetSubtargetInfo.h"
#include <string>

#define GET_SUBTARGETINFO_HEADER
#include "TinyGenSubtargetInfo.inc"

namespace llvm {
class StringRef;

class TinySubtarget : public TinyGenSubtargetInfo {
  virtual void anchor();
  bool ExtendedInsts;
  //const DataLayout DL; // Calculates type size & alignment
  TinyFrameLowering FrameLowering;
  TinyInstrInfo InstrInfo;
  TinyTargetLowering TLInfo;
  SelectionDAGTargetInfo TSInfo;

public:
  /// This constructor initializes the data members to match that
  /// of the specified triple.
  ///
  TinySubtarget(const Triple &TT, const std::string &CPU,
                  const std::string &FS, const TargetMachine &TM);

  TinySubtarget &initializeSubtargetDependencies(StringRef CPU, StringRef FS);

  /// ParseSubtargetFeatures - Parses features string setting specified
  /// subtarget options.  Definition of function is auto generated by tblgen.
  void ParseSubtargetFeatures(StringRef CPU, StringRef FS);

  const TargetFrameLowering *getFrameLowering() const { return &FrameLowering; }
  const TinyInstrInfo *getInstrInfo() const { return &InstrInfo; }
  //const DataLayout *getDataLayout() const { return &DL; }
  const TargetRegisterInfo *getRegisterInfo() const {
    return &InstrInfo.getRegisterInfo();
  }
  const TinyTargetLowering *getTargetLowering() const { return &TLInfo; }
  const SelectionDAGTargetInfo *getSelectionDAGInfo() const { return &TSInfo; }
};
} // End llvm namespace

#endif  // LLVM_TARGET_Tiny_SUBTARGET_H
