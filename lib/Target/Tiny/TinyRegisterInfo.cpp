//===-- TinyRegisterInfo.cpp - Tiny Register Information --------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the Tiny implementation of the TargetRegisterInfo class.
//
//===----------------------------------------------------------------------===//

#include "TinyRegisterInfo.h"
#include "Tiny.h"
#include "TinyMachineFunctionInfo.h"
#include "TinyTargetMachine.h"
#include "llvm/ADT/BitVector.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"

#include "llvm/Support/Debug.h"


using namespace llvm;

#define DEBUG_TYPE "Tiny-reg-info"

#define GET_REGINFO_TARGET_DESC
#include "TinyGenRegisterInfo.inc"

// FIXME: Provide proper call frame setup / destroy opcodes.
TinyRegisterInfo::TinyRegisterInfo()
    // pass R0 as return address register
  : TinyGenRegisterInfo(Tiny::PC) {}

/* FIXME do we need this?
const uint32_t*
TinyRegisterInfo::getCallPreservedMask(CallingConv::ID) const {
    return CSR_RegMask;
}
*/

const MCPhysReg*
TinyRegisterInfo::getCalleeSavedRegs(const MachineFunction *MF) const {
  // const TargetFrameLowering *TFI = MF->getTarget().getFrameLowering();

  /*
  if (TFI->hasFP(*MF))
    return (F->getCallingConv() == CallingConv::MSP430_INTR ?
            CalleeSavedRegsIntrFP : CalleeSavedRegsFP);
  else
    return (F->getCallingConv() == CallingConv::MSP430_INTR ?
            CalleeSavedRegsIntr : CalleeSavedRegs);
  */

  return CSR_SaveList;
}

BitVector TinyRegisterInfo::getReservedRegs(const MachineFunction &MF) const {
    static const uint16_t ReservedCPURegs[] = {
        Tiny::SP
    };
    BitVector Reserved(getNumRegs());

    for (unsigned i = 0; i < array_lengthof(ReservedCPURegs); i++)
        Reserved.set(ReservedCPURegs[i]);

    if (MF.getSubtarget().getFrameLowering()->hasFP(MF))
        Reserved.set(Tiny::FP);

    return Reserved;
}

const TargetRegisterClass *
TinyRegisterInfo::getPointerRegClass(const MachineFunction &MF, unsigned Kind) const {
    return &Tiny::CPURegsRegClass;
}

bool TinyRegisterInfo::needsStackRealignment(const MachineFunction &MF) const {
  const MachineFrameInfo *MFI = MF.getFrameInfo();
  //const Function *F = MF.getFunction();
  unsigned StackAlign = MF.getSubtarget().getFrameLowering()->getStackAlignment();
  // FIXME: what about this?
  //bool requiresRealignment =
  //  ((MFI->getMaxAlignment() > StackAlign) ||
  //   F->getAttributes().hasAttribute(AttributeSet::FunctionIndex,
  //                                   Attribute::StackAlignment));
  return MFI->getMaxAlignment() > StackAlign;
}

void
TinyRegisterInfo::eliminateFrameIndex(MachineBasicBlock::iterator II,
                                        int SPAdj, unsigned FIOperandNum,
                                        RegScavenger *RS) const {
  assert(SPAdj == 0 && "Unexpected");

  MachineInstr &MI = *II;
  MachineBasicBlock &MBB = *MI.getParent();
  MachineFunction &MF = *MBB.getParent();
  const TargetFrameLowering *TFI = MF.getSubtarget().getFrameLowering();
  const TargetRegisterInfo *TRI = MF.getSubtarget().getRegisterInfo();
  //DebugLoc dl = MI.getDebugLoc();
  int FrameIndex = MI.getOperand(FIOperandNum).getIndex();

  uint64_t stackSize = MF.getFrameInfo()->getStackSize();
  int Offset = MF.getFrameInfo()->getObjectOffset(FrameIndex);
  unsigned BasePtr = (TFI->hasFP(MF) ? Tiny::FP : Tiny::SP);

  if (TRI->needsStackRealignment(MF)) {
    if (FrameIndex >= 0) {
        BasePtr = Tiny::SP;
        Offset += stackSize;
    }
  }

  if (!TFI->hasFP(MF))
    Offset += stackSize;

  errs() << "\nFunction : " << MF.getName() << "\n";
        errs() << "<--------->\n" << MI;

  errs() << "FrameIndex : " << FrameIndex << "\n"
               << "spOffset   : " << Offset << "\n"
               << "stackSize  : " << stackSize << "\n";

  // Fold imm into offset
  Offset += MI.getOperand(FIOperandNum + 1).getImm();
  /* //if (MI.getOpcode() == Tiny::ADD16ri) {
      // This is actually "load effective address" of the stack slot
    // instruction. We have only two-address instructions, thus we need to
    // expand it into mov + add
    const TargetInstrInfo &TII = *MF.getTarget().getInstrInfo();

    MI.setDesc(TII.get(Tiny::MOV16rr));
 //   MI.getOperand(FIOperandNum).ChangeToRegister(BasePtr, false);

    if (Offset == 0)
      return;
    // We need to materialize the offset via add instruction.
    unsigned DstReg = MI.getOperand(0).getReg();
    if (Offset < 0)
      BuildMI(MBB, std::next(II), dl, TII.get(Tiny::SUB16ri), DstReg)
        .addReg(DstReg).addImm(-Offset);
    else
      BuildMI(MBB, std::next(II), dl, TII.get(Tiny::ADD16ri), DstReg)
        .addReg(DstReg).addImm(Offset);
    return;
  }
*/
  MI.getOperand(FIOperandNum).ChangeToRegister(BasePtr, false);
  MI.getOperand(FIOperandNum + 1).ChangeToImmediate(Offset);
}

unsigned TinyRegisterInfo::getFrameRegister(const MachineFunction &MF) const {
  const TargetFrameLowering *TFI = MF.getSubtarget().getFrameLowering();

  //return Tiny::SP 
  return TFI->hasFP(MF) ? Tiny::FP : Tiny::SP;
}
