//===-- TinySubtarget.cpp - Tiny Subtarget Information ----------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements the Tiny specific subclass of TargetSubtargetInfo.
//
//===----------------------------------------------------------------------===//

#include "TinySubtarget.h"
#include "Tiny.h"
#include "llvm/Support/TargetRegistry.h"

using namespace llvm;

#define DEBUG_TYPE "Tiny-subtarget"

#define GET_SUBTARGETINFO_TARGET_DESC
#define GET_SUBTARGETINFO_CTOR
#include "TinyGenSubtargetInfo.inc"

void TinySubtarget::anchor() { }

TinySubtarget &TinySubtarget::initializeSubtargetDependencies(StringRef CPU, StringRef FS) {
  ParseSubtargetFeatures("generic", FS);
  return *this;
}

TinySubtarget::TinySubtarget(const Triple &TT, const std::string &CPU,
                                 const std::string &FS, const TargetMachine &TM)
    : TinyGenSubtargetInfo(TT, CPU, FS),
      // provide data layout info for TinyMachine
      // e - little endian
      // m:e - mangling (asm symbols prefix)
      // p:32:32 - pointer size and alignments
      // i32:32:32 - inte alignment
      // n32 - native int sizes
      //DL("e-m:e-p:32:32-i32:32:32-n32"),
      FrameLowering(),
      InstrInfo(initializeSubtargetDependencies(CPU, FS)),
      TLInfo(TM, *this) {}
      //TSInfo(DL) {}
