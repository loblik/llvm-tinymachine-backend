//===-- TinyMCTargetDesc.cpp - Tiny Target Descriptions ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file provides Tiny specific target descriptions.
//
//===----------------------------------------------------------------------===//

#include "TinyMCTargetDesc.h"
#include "InstPrinter/TinyInstPrinter.h"
#include "TinyMCAsmInfo.h"
#include "llvm/MC/MCInstrInfo.h"
#include "llvm/MC/MCRegisterInfo.h"
#include "llvm/MC/MCSubtargetInfo.h"
#include "llvm/Support/TargetRegistry.h"

using namespace llvm;

#define GET_INSTRINFO_MC_DESC
#include "TinyGenInstrInfo.inc"

#define GET_SUBTARGETINFO_MC_DESC
#include "TinyGenSubtargetInfo.inc"

#define GET_REGINFO_MC_DESC
#include "TinyGenRegisterInfo.inc"

static MCInstrInfo *createTinyMCInstrInfo() {
  MCInstrInfo *X = new MCInstrInfo();
  InitTinyMCInstrInfo(X);
  return X;
}

static MCRegisterInfo *createTinyMCRegisterInfo(const Triple &TT) {
  MCRegisterInfo *X = new MCRegisterInfo();
  // return address register R0?
  InitTinyMCRegisterInfo(X, Tiny::R0);
  return X;
}

static MCSubtargetInfo *createTinyMCSubtargetInfo(const Triple &TT, StringRef CPU,
                                                    StringRef FS) {
  return createTinyMCSubtargetInfoImpl(TT, CPU, FS);
  //MCSubtargetInfo *X = new MCSubtargetInfo();
  //InitTinyMCSubtargetInfo(X, TT, CPU, FS);
  //return X;
}

/*
static MCCodeGenInfo *createTinyMCCodeGenInfo(StringRef TT, Reloc::Model RM,
                                                CodeModel::Model CM,
                                                CodeGenOpt::Level OL) {
  MCCodeGenInfo *X = new MCCodeGenInfo();
  X->InitMCCodeGenInfo(RM, CM, OL);
  return X;
}
*/

static MCInstPrinter *createTinyMCInstPrinter(const Triple &T,
                                              unsigned SyntaxVariant,
                                              const MCAsmInfo &MAI,
                                              const MCInstrInfo &MII,
                                              const MCRegisterInfo &MRI) {
  if (SyntaxVariant == 0)
    return new TinyInstPrinter(MAI, MII, MRI);
  return nullptr;
}

extern "C" void LLVMInitializeTinyTargetMC() {
  // Register the MC asm info.
  RegisterMCAsmInfo<TinyMCAsmInfo> X(TheTinyTarget);

  // Register the MC codegen info.
  //TargetRegistry::RegisterMCCodeGenInfo(TheTinyTarget,
  //                                      createTinyMCCodeGenInfo);

  // Register the MC instruction info.
  TargetRegistry::RegisterMCInstrInfo(TheTinyTarget, createTinyMCInstrInfo);

  // Register the MC register info.
  TargetRegistry::RegisterMCRegInfo(TheTinyTarget,
                                    createTinyMCRegisterInfo);

  // Register the MC subtarget info.
  TargetRegistry::RegisterMCSubtargetInfo(TheTinyTarget,
                                          createTinyMCSubtargetInfo);

  // Register the MCInstPrinter.
  TargetRegistry::RegisterMCInstPrinter(TheTinyTarget,
                                        createTinyMCInstPrinter);
}
