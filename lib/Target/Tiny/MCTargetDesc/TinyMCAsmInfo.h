//===-- TinyMCAsmInfo.h - Tiny asm properties --------------*- C++ -*--===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source 
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the declaration of the TinyMCAsmInfo class.
//
//===----------------------------------------------------------------------===//

#ifndef TinyTARGETASMINFO_H
#define TinyTARGETASMINFO_H

//#include "llvm/MC/MCAsmInfoELF.h"
#include "llvm/MC/MCAsmInfoCOFF.h"

namespace llvm {
  class Triple;

  class TinyMCAsmInfo : public MCAsmInfoCOFF {
    void anchor() override;
  public:
    explicit TinyMCAsmInfo(const Triple &TT);
  };

} // namespace llvm

#endif
