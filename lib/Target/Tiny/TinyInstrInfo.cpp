//===-- TinyInstrInfo.cpp - Tiny Instruction Information --------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the Tiny implementation of the TargetInstrInfo class.
//
//===----------------------------------------------------------------------===//

#include "TinyInstrInfo.h"
#include "Tiny.h"
#include "TinyMachineFunctionInfo.h"
#include "TinyTargetMachine.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/TargetRegistry.h"

using namespace llvm;

#define GET_INSTRINFO_CTOR_DTOR
#include "TinyGenInstrInfo.inc"

// Pin the vtable to this file.
void TinyInstrInfo::anchor() {}

TinyInstrInfo::TinyInstrInfo(TinySubtarget &STI)
  : TinyGenInstrInfo(Tiny::ADJCALLSTACKDOWN, Tiny::ADJCALLSTACKUP),
    RI() {}

unsigned TinyInstrInfo::isStoreToStackSlot(const MachineInstr *MI,
                                            int &FrameIndex) const {
//  return 0;
  if (MI->getOpcode() == Tiny::ST_ri ||
      MI->getOpcode() == Tiny::STB_ri ||
      MI->getOpcode() == Tiny::STH_ri) {
    // [ FI + Imm ] = Reg
    if (MI->getOperand(0).isFI() && MI->getOperand(1).isImm() &&
        MI->getOperand(1).getImm() == 0) {
        FrameIndex = MI->getOperand(0).getIndex();
        return MI->getOperand(2).getReg();
    }
  }
  return 0;
}

unsigned TinyInstrInfo::isLoadFromStackSlot(const MachineInstr *MI,
                                            int &FrameIndex) const {
// return 0;
  if (MI->getOpcode() == Tiny::LD_ri) {
    MI->dump();
    // Reg = [ FI + Imm ]
    if (MI->getOperand(1).isFI() && MI->getOperand(2).isImm() &&
      MI->getOperand(2).getImm() == 0) {
      FrameIndex = MI->getOperand(1).getIndex();
      return MI->getOperand(0).getReg();
    }
  }
  return 0;
}

/* to support spilling */
void TinyInstrInfo::storeRegToStackSlot(MachineBasicBlock &MBB,
                                          MachineBasicBlock::iterator MI,
                                    unsigned SrcReg, bool isKill, int FrameIdx,
                                          const TargetRegisterClass *RC,
                                          const TargetRegisterInfo *TRI) const {
  DebugLoc DL;
  if (MI != MBB.end()) DL = MI->getDebugLoc();
  MachineFunction &MF = *MBB.getParent();
  MachineFrameInfo &MFI = *MF.getFrameInfo();

  MachineMemOperand *MMO =
    MF.getMachineMemOperand(MachinePointerInfo::getFixedStack(MF, FrameIdx),
                            MachineMemOperand::MOStore,
                            MFI.getObjectSize(FrameIdx),
                            MFI.getObjectAlignment(FrameIdx));

  if (RC == &Tiny::CPURegsRegClass)
    BuildMI(MBB, MI, DL, get(Tiny::ST_ri))
      .addReg(SrcReg, getKillRegState(isKill))
      .addFrameIndex(FrameIdx)
      .addImm(0)
      .addMemOperand(MMO);
/*
  if (RC == &Tiny::CPURegsRegClass)
    BuildMI(MBB, MI, DL, get(Tiny::ST_ri))
      .addReg(SrcReg, getKillRegState(isKill))
      .addFrameIndex(FrameIdx)
      .addImm(0)
      .addMemOperand(MMO);

  else if (RC == &Tiny::GR8RegClass)
    BuildMI(MBB, MI, DL, get(Tiny::MOV8mr))
      .addFrameIndex(FrameIdx).addImm(0)
      .addReg(SrcReg, getKillRegState(isKill)).addMemOperand(MMO);
 */
  else
    llvm_unreachable("Cannot store this register to stack slot!");
}

void TinyInstrInfo::loadRegFromStackSlot(MachineBasicBlock &MBB,
                                           MachineBasicBlock::iterator MI,
                                           unsigned DestReg, int FrameIdx,
                                           const TargetRegisterClass *RC,
                                           const TargetRegisterInfo *TRI) const{
  DebugLoc DL;
  if (MI != MBB.end()) DL = MI->getDebugLoc();
  MachineFunction &MF = *MBB.getParent();
  MachineFrameInfo &MFI = *MF.getFrameInfo();

  MachineMemOperand *MMO =
    MF.getMachineMemOperand(MachinePointerInfo::getFixedStack(MF, FrameIdx),
                            MachineMemOperand::MOLoad,
                            MFI.getObjectSize(FrameIdx),
                            MFI.getObjectAlignment(FrameIdx));

  if (RC == &Tiny::CPURegsRegClass)
    BuildMI(MBB, MI, DL, get(Tiny::LD_ri), DestReg).addFrameIndex(FrameIdx).addImm(0).addMemOperand(MMO);
/*
  else if (RC == &Tiny::GR8RegClass)
    BuildMI(MBB, MI, DL, get(Tiny::MOV8rm))
      .addReg(DestReg).addFrameIndex(FrameIdx).addImm(0).addMemOperand(MMO);
*/
  else
    llvm_unreachable("Cannot store this register to stack slot!");
}

#include "llvm/Support/Debug.h"

void TinyInstrInfo::copyPhysReg(MachineBasicBlock &MBB,
                                  MachineBasicBlock::iterator I, const DebugLoc &DL,
                                  unsigned DestReg, unsigned SrcReg,
                                  bool KillSrc) const {

    DEBUG_WITH_TYPE("backend", dbgs() << "TinyInstrInfo::copyPhysReg called\n");

  unsigned Opc;
  if (Tiny::CPURegsRegClass.contains(DestReg, SrcReg))
    Opc = Tiny::MOV;
//  else if (Tiny::GR8RegClass.contains(DestReg, SrcReg))
//    Opc = Tiny::MOV8rr;
  else
    llvm_unreachable("Impossible reg-to-reg copy");

  BuildMI(MBB, I, DL, get(Opc), DestReg)
    .addReg(SrcReg, getKillRegState(KillSrc));
}

unsigned TinyInstrInfo::RemoveBranch(MachineBasicBlock &MBB) const {
  DEBUG_WITH_TYPE("backend", dbgs() << "TinyInstrInfo::RemoveBranch called\n");

  MachineBasicBlock::iterator I = MBB.end();
  unsigned Count = 0;


  while (I != MBB.begin()) {
    --I;
    if (I->isDebugValue())
      continue;
    if (I->getOpcode() != Tiny::JNE &&
        I->getOpcode() != Tiny::JEQ &&
        I->getOpcode() != Tiny::JMP &&
        I->getOpcode() != Tiny::JLE &&
        I->getOpcode() != Tiny::JGE &&
        I->getOpcode() != Tiny::JLT &&
        I->getOpcode() != Tiny::JGT)
      break;
    // Remove the branch.
    errs() << "BRANCH: removing ";
    I->dump();
    I->eraseFromParent();
    I = MBB.end();
    ++Count;
  }

  return Count;
  return 0;
}

bool TinyInstrInfo::
ReverseBranchCondition(SmallVectorImpl<MachineOperand> &Cond) const {
  DEBUG_WITH_TYPE("backend", dbgs() << "TinyInstrInfo::ReverseBranchCondition called\n");
  //assert(0 && "not implemented");

  assert(Cond.size() == 2 && "Invalid Xbranch condition!");

  TinyCC::CondCodes CC = static_cast<TinyCC::CondCodes>(Cond[0].getImm());

  unsigned reversedCond;

  switch (Cond[0].getImm()) {
        case Tiny::JNE:
            reversedCond = Tiny::JEQ;
            break;
        case Tiny::JEQ:
            reversedCond = Tiny::JNE;
            break;
        case Tiny::JGE:
            reversedCond = Tiny::JLT;
            break;
        case Tiny::JGT:
            reversedCond = Tiny::JLE;
            break;
        case Tiny::JLT:
            reversedCond = Tiny::JGE;
            break;
        case Tiny::JLE:
            reversedCond = Tiny::JGT;
            break;
        default: return true;
  }

  Cond[0].setImm(reversedCond);
  return false;
}

bool TinyInstrInfo::isUnpredicatedTerminator(const MachineInstr &MI) const {
  DEBUG_WITH_TYPE("backend", dbgs() << "TinyInstrInfo::isUnpredicatedTerminator called\n");
  if (!MI.isTerminator()) return false;

  // Conditional branch is a special case.
  if (MI.isBranch() && !MI.isBarrier())
    return true;
  if (!MI.isPredicable())
    return true;
  return !isPredicated(MI);
}

//bool AnalyzeBranch(MachineBasicBlock &MBB,
//                     MachineBasicBlock *&TBB, MachineBasicBlock *&FBB,
//                     SmallVectorImpl<MachineOperand> &Cond,
//                     bool AllowModify) const override;


bool TinyInstrInfo::analyzeBranch(MachineBasicBlock &MBB,
                                    MachineBasicBlock *&TBB,
                                    MachineBasicBlock *&FBB,
                                    SmallVectorImpl<MachineOperand> &Cond,
                                    bool AllowModify) const {
  /* FIXME: we need to implement this to support branch folding, see
   * ARM backend for example */
  DEBUG_WITH_TYPE("backend", dbgs() << "TinyInstrInfo::analyzeBranch called\n");
//  return true;
  if (getenv("NOBRANCH"))
    return true;

  errs() << "BRANCH: Analyze Branch\n";
    MBB.dump();


  // Start from the bottom of the block and work up, examining the
  // terminator instructions.
  MachineBasicBlock::iterator I = MBB.end();
  while (I != MBB.begin()) {
    --I;
    if (I->isDebugValue())
      continue;

    // Working from the bottom, when we see a non-terminator
    // instruction, we're done.
    if (!isUnpredicatedTerminator(*I))
      break;

    // A terminator that isn't a branch can't easily be handled
    // by this analysis.
    if (!I->isBranch())
      return true;


    errs() << "BRANCH: I->dump()\n";
    I->dump();

    // Cannot handle indirect branches.
    // Tiny doesn't have indirect branches yet
    // if (I->getOpcode() == Tiny::Br ||
    //     I->getOpcode() == Tiny::Bm)
    //   return true;

    // Handle unconditional branches.
    if (I->getOpcode() == Tiny::JMP) {
      if (!AllowModify) {
        TBB = I->getOperand(0).getMBB();
        // FIXME
        continue;
      }

      // FIXME
      // If the block has any instructions after a JMP, delete them.
      /*
      while (std::next(I) != MBB.end())
        std::next(I)->eraseFromParent();
      Cond.clear();
      FBB = nullptr;
      */

      // Delete the JMP if it's equivalent to a fall-through.
      //if (MBB.isLayoutSuccessor(I->getOperand(0).getMBB())) {
      //  TBB = nullptr;
      //  I->eraseFromParent();
      //  I = MBB.end();
      //  continue;
      //}

      // TBB is used to indicate the unconditinal destination.
      TBB = I->getOperand(0).getMBB();
      continue;
    } // JMP

// Handle conditional branches.
//    assert(I->getOpcode() == Tiny::JCC && "Invalid conditional branch");
//    TinyCC::CondCodes BranchCode =
//      static_cast<TinyCC::CondCodes>(I->getOperand(1).getImm());
//    if (BranchCode == TinyCC::COND_INVALID)
//      return true;  // Can't handle weird stuff.

    switch (I->getOpcode()) {
        case Tiny::JNE:
        case Tiny::JEQ:
        case Tiny::JGE:
        case Tiny::JGT:
        case Tiny::JLT:
        case Tiny::JLE: break;
        default: assert(0 && "jump opcode expected");
    }


    // Working from the bottom, handle the first conditional branch.
    if (Cond.empty()) {
      FBB = TBB;
      TBB = I->getOperand(1).getMBB();
      Cond.push_back(MachineOperand::CreateImm(I->getOpcode()));
      Cond.push_back(MachineOperand::CreateImm(I->getOperand(0).getReg()));
      continue;
    }

    MBB.dump();
    assert(0 && "BB with multiple branches? wtf?");

    // Handle subsequent conditional branches. Only handle the case where all
    // conditional branches branch to the same destination.
    //assert(Cond.size() == 1);
    //assert(TBB);

    // Only handle the case where all conditional branches branch to
    // the same destination.
    if (TBB != I->getOperand(1).getMBB())
      return true;

//    TinyCC::CondCodes OldBranchCode = (TinyCC::CondCodes)Cond[0].getImm();
    // If the conditions are the same, we can leave them alone.
 //   if (OldBranchCode == BranchCode)
  //    continue;

    return true;
  }


  errs() << "BRANCH: AnalyzeBranch end\n";

  return false;
}

unsigned
TinyInstrInfo::InsertBranch(MachineBasicBlock &MBB, MachineBasicBlock *TBB,
                              MachineBasicBlock *FBB,
                              ArrayRef<MachineOperand> Cond,
                              const DebugLoc &DL) const {
  // assert(0 && "not implemented");
  DEBUG_WITH_TYPE("backend", dbgs() << "TinyInstrInfo::InsertBranch called\n");

  // Shouldn't be a fall through.
  assert(TBB && "InsertBranch must not be told to insert a fallthrough");
  assert((Cond.size() == 2 || Cond.size() == 0) &&
         "Tiny branch conditions have one component!");

  if (Cond.empty()) {
    // Unconditional branch?
    assert(!FBB && "Unconditional branch with multiple successors!");
    errs() << "\nInserting JMP to " << TBB->getFullName() << " in " << MBB.getFullName();
    BuildMI(&MBB, DL, get(Tiny::JMP)).addMBB(TBB);
    return 1;
  }

  //assert(0 && "unconditional branches not implemented yet");

  errs() << "\nInserting CBR to " << TBB->getFullName() << " in " << MBB.getFullName();
  // Conditional branch.
  unsigned Count = 0;
  BuildMI(&MBB, DL, get(Cond[0].getImm())).addReg(Cond[1].getImm()).addMBB(TBB);
  ++Count;

  if (FBB) {
    // Two-way Conditional branch. Insert the second branch.
    BuildMI(&MBB, DL, get(Tiny::JMP)).addMBB(FBB);
    ++Count;
  }
  return Count;
}

/// GetInstSize - Return the number of bytes of code the specified
/// instruction may be.  This returns the maximum number of bytes.
///
unsigned TinyInstrInfo::GetInstSizeInBytes(const MachineInstr *MI) const {
  const MCInstrDesc &Desc = MI->getDesc();

  switch (Desc.TSFlags & TinyII::SizeMask) {
  default:
    switch (Desc.getOpcode()) {
    default: llvm_unreachable("Unknown instruction size!");
    case TargetOpcode::CFI_INSTRUCTION:
    case TargetOpcode::EH_LABEL:
    case TargetOpcode::IMPLICIT_DEF:
    case TargetOpcode::KILL:
    case TargetOpcode::DBG_VALUE:
      return 0;
    case TargetOpcode::INLINEASM: {
      const MachineFunction *MF = MI->getParent()->getParent();
      const TargetInstrInfo &TII = *MF->getSubtarget().getInstrInfo();
      return TII.getInlineAsmLength(MI->getOperand(0).getSymbolName(),
                                    *MF->getTarget().getMCAsmInfo());
    }
    }
  case TinyII::SizeSpecial:
    switch (MI->getOpcode()) {
    default: llvm_unreachable("Unknown instruction size!");
   // case Tiny::SAR8r1c:
   // case Tiny::SAR16r1c:
   //   return 4;
    }
  case TinyII::Size2Bytes:
    return 2;
  case TinyII::Size4Bytes:
    return 4;
  case TinyII::Size6Bytes:
    return 6;
  }
}

bool TinyInstrInfo::expandPostRAPseudo(MachineInstr &MI) const {
  MachineBasicBlock &MBB = *MI.getParent();
  MachineFunction &MF = *MBB.getParent();

  const TinyInstrInfo &TII = *static_cast<const TinyInstrInfo*>(MF.getSubtarget().getInstrInfo());

  MachineInstr *Old = &MI;
  //bool isMicroMips = Subtarget.inMicroMipsMode();
  //unsigned Opc;
  switch(MI.getDesc().getOpcode()) {
  default:
    return false;
    //case Mips::RetRA:
    case Tiny::CALLimm: {
        MachineInstr *loadAddr = BuildMI(MF, Old->getDebugLoc(), TII.get(Tiny::LDCsym), Tiny::R1).addOperand(MI.getOperand(0));
        MBB.insert(MI, loadAddr);
        MachineInstr *callInst = BuildMI(MF, Old->getDebugLoc(), TII.get(Tiny::CALLreg), Tiny::R1);
        MBB.insert(MI, callInst);
        }
        break;
    case Tiny::LEA: {
        unsigned reg_tgt = MI.getOperand(0).getReg();
        unsigned reg_src = MI.getOperand(1).getReg();
        int64_t imm = MI.getOperand(2).getImm();
        if (imm) {
            MachineInstr *loadCons = BuildMI(MF, Old->getDebugLoc(), TII.get(Tiny::LDC), reg_tgt)
                .addImm(imm);
            MBB.insert(MI, loadCons);
            MachineInstr *add = BuildMI(MF, Old->getDebugLoc(), TII.get(Tiny::ADD),
                reg_tgt).addReg(reg_tgt).addReg(reg_src);
            MBB.insert(MI, add);
        } else {
            /* no need to add if imm is zero */
            MachineInstr *move = BuildMI(MF, Old->getDebugLoc(), TII.get(Tiny::MOV), reg_tgt)
                .addReg(reg_src);
            MBB.insert(MI, move);
        }
        }
    break;
  }
  MBB.erase(MI);
  return true;
}

